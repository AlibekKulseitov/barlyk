//'use strict';

$(function() {

    /*
    |--------------------------------------------------------------------------
    | Chat Scripts
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Account Menu
    |--------------------------------------------------------------------------
    */

    // Chat Helper
    // declare variable
    const chatHelperButton = $('.js-chat-helper');
    const chatHelper = $('.chat__dropdown');

    // toggle dropdown
    function chatHelperToggle() {
        chatHelperButton.on('click touchstart', function () {
            chatHelper.toggleClass('--active');
        })
    }

    // call function
    chatHelperToggle();

    // Emoji Dropdown
    // declare variable
    const emojiButton = $('.js-emoji-button');
    const emojiDropdown = $('.chat__emoji-dropdown');

    // toggle dropdown
    function emojiToggle() {
        emojiButton.on('click touchstart', function () {
            emojiDropdown.toggleClass('--active');
        })
    }

    // call function
    emojiToggle();

    // Photos Dropdown
    // declare variable
    const photosButton = $('.js-photos-button');
    const photosDropdown = $('.chat__photos-dropdown');

    // toggle dropdown
    function photosToggle() {
        photosButton.on('click touchstart', function () {
            photosDropdown.toggleClass('--active');
        })
    }

    // call function
    photosToggle();

    // close dropdown on document click
    $(document).on('click touchstart', function(e) {
        //e.stopPropagation();
        const target = $(e.target);

        if (!$(e.target).closest(photosButton).length) {
            photosDropdown.removeClass('--active');
        }

        if (!$(e.target).closest(emojiButton).length) {
            emojiDropdown.removeClass('--active');
        }

        if (!$(e.target).closest(chatHelperButton).length) {
            chatHelper.removeClass('--active');
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Dropdown
    |--------------------------------------------------------------------------
    */

    $('.dropdown-menu').on('click', function(event){
        var events = $._data(document, 'events') || {};
        events = events.click || [];
        for(var i = 0; i < events.length; i++) {
            if(events[i].selector) {

                //Check if the clicked element matches the event selector
                if($(event.target).is(events[i].selector)) {
                    events[i].handler.call(event.target, event);
                }

                // Check if any of the clicked element parents matches the
                // delegated event selector (Emulating propagation)
                $(event.target).parents(events[i].selector).each(function(){
                    events[i].handler.call(this, event);
                });
            }
        }
        event.stopPropagation(); //Always stop propagation
    });

    /*
    |--------------------------------------------------------------------------
    | User Menu
    |--------------------------------------------------------------------------
    */

    var userMenuToggle = $('.js-user-menu-toggle');

    // User Menu toggle to sidebar-folded
    userMenuToggle.on('click', function(e) {

        // Toggle Open Class
        $(this).toggleClass('is-open');

        e.preventDefault();

        $('body').toggleClass('user-menu-modal-open');

    });

    // close sidebar when click outside on mobile/table
    $(document).on('click touchstart', function(e){
        e.stopPropagation();

        // closing of sidebar menu when clicking outside of it
        if (!$(e.target).closest(userMenuToggle).length) {
            var userMenu = $(e.target).closest('.user-menu-modal').length;
            var userMenuBody = $(e.target).closest('.user-menu-modal__body').length;
            if (!userMenu && !userMenuBody) {
                if ($('body').hasClass('user-menu-modal-open')) {
                    $('body').removeClass('user-menu-modal-open');
                }
            }
        }
    });


    /*
    |--------------------------------------------------------------------------
    | Mobile Menu
    |--------------------------------------------------------------------------
    */

    var burger = $('.js-menu-trigger');

    // Sidebar toggle to sidebar-folded
    burger.on('click', function(e) {

        // Toggle Open Class
        $(this).toggleClass('is-open');

        e.preventDefault();

        $('body').toggleClass('mmenu-open');

    });

    // close sidebar when click outside on mobile/table
    $(document).on('click touchstart', function(e){
        e.stopPropagation();

        // closing of sidebar menu when clicking outside of it
        if (!$(e.target).closest(burger).length) {
            var sidebar = $(e.target).closest('.m-menu').length;
            var sidebarBody = $(e.target).closest('.m-menu__body').length;
            if (!sidebar && !sidebarBody) {
                if ($('body').hasClass('mmenu-open')) {
                    $('body').removeClass('mmenu-open');
                }
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Search Modal
    |--------------------------------------------------------------------------
    */

    var search = $('.js-search-modal');

    // Sidebar toggle to sidebar-folded
    search.on('click', function(e) {

        // Toggle Open Class
        $(this).toggleClass('is-open');

        e.preventDefault();

        $('body').toggleClass('search-open');

    });

    // close sidebar when click outside on mobile/table
    $(document).on('click touchstart', function(e){
        e.stopPropagation();

        // closing of sidebar menu when clicking outside of it
        if (!$(e.target).closest(search).length) {
            var sidebar = $(e.target).closest('.search-modal').length;
            var sidebarBody = $(e.target).closest('.search-modal__body').length;
            if (!sidebar && !sidebarBody) {
                if ($('body').hasClass('search-open')) {
                    $('body').removeClass('search-open');
                }
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Sticky Kit
    |--------------------------------------------------------------------------
    */

    if(window.matchMedia('(min-width: 992px)').matches) {
        $(".js-sticky").stick_in_parent({
            recalc_every: 1,
            offset_top: 50,
        });
    }

    /*
    |--------------------------------------------------------------------------
    | // https://www.coralnodes.com/hide-header-on-scroll-down/
    |--------------------------------------------------------------------------
    */

    if ($('#header').length > 0) {

        var doc = document.documentElement;
        var w = window;

        var prevScroll = w.scrollY || doc.scrollTop;
        var curScroll;
        var direction = 0;
        var prevDirection = 0;
        var headerHeight = $("#header").height();

        var header = document.getElementById('header');

        var checkScroll = function() {

            /*
            ** Find the direction of scroll
            ** 0 - initial, 1 - up, 2 - down
            */

            curScroll = w.scrollY || doc.scrollTop;
            if (curScroll > prevScroll) {
                //scrolled up
                direction = 2;
            }
            else if (curScroll < prevScroll) {
                //scrolled down
                direction = 1;
            }

            if (direction !== prevDirection) {
                toggleHeader(direction, curScroll);
            }

            prevScroll = curScroll;
        };

        var toggleHeader = function(direction, curScroll) {
            if (direction === 2 && curScroll > headerHeight) {
                header.classList.add('is-hidden');
                prevDirection = direction;
            }
            else if (direction === 1) {
                header.classList.remove('is-hidden');
                prevDirection = direction;
            }
        };

        window.addEventListener('scroll', checkScroll);

    }

   /*
   |--------------------------------------------------------------------------
   | Showing modal with effect
   |--------------------------------------------------------------------------
   */

    $('.js-modal-effect').on('click', function(e) {

        e.preventDefault();

        var effect = $(this).attr('data-effect');
        $('.modal').addClass(effect);
    });
    // hide modal with effect

    /*
    |--------------------------------------------------------------------------
    | Mobile Menu
    |--------------------------------------------------------------------------
    */

    var burger = $('.js-menu-toggle');

    // Sidebar toggle to sidebar-folded
    burger.on('click', function(e) {

        // Toggle Open Class
        $(this).toggleClass('is-open');

        e.preventDefault();

        $('body').toggleClass('mmenu-open');

    });

    // close sidebar when click outside on mobile/table
    $(document).on('click touchstart', function(e){
        e.stopPropagation();

        // closing of sidebar menu when clicking outside of it
        if (!$(e.target).closest(burger).length) {
            var sidebar = $(e.target).closest('.m-menu').length;
            var sidebarBody = $(e.target).closest('.m-menu__body').length;
            if (!sidebar && !sidebarBody) {
                if ($('body').hasClass('mmenu-open')) {
                    $('body').removeClass('mmenu-open');
                }
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Responsive Iframe Inside Modal
    |--------------------------------------------------------------------------
    */

    function toggle_video_modal() {

        // Click on video thumbnail or link
        $(".js-video-modal").on("click", function(e){

            // prevent default behavior for a-tags, button tags, etc.
            e.preventDefault();

            // Grab the video ID from the element clicked
            var id = $(this).attr('data-youtube-id');

            // Autoplay when the modal appears
            // Note: this is intetnionally disabled on most mobile devices
            // If critical on mobile, then some alternate method is needed
            var autoplay = '?autoplay=1';

            // Don't show the 'Related Videos' view when the video ends
            var related_no = '&rel=0';

            // String the ID and param variables together
            var src = '//www.youtube.com/embed/'+id+autoplay+related_no;

            // Pass the YouTube video ID into the iframe template...
            // Set the source on the iframe to match the video ID
            $(".video-modal__iframe").attr('src', src);

            // Add class to the body to visually reveal the modal
            $("body").addClass("video-modal-show");

            $('body').css({"overflow": "hidden"});

        });

        // Close and Reset the Video Modal
        function close_video_modal() {

            event.preventDefault();

            // re-hide the video modal
            $("body").removeClass("video-modal-show");

            $('body').css({"overflow": ""});

            // reset the source attribute for the iframe template, kills the video
            $(".video-modal__iframe").attr('src', '');

        }
        // if the 'close' button/element, or the overlay are clicked
        $('body').on('click', '.video-modal__close, .video-modal__overlay', function(event) {

            // call the close and reset function
            close_video_modal();

        });
        // if the ESC key is tapped
        $('body').keyup(function(e) {
            // ESC key maps to keycode `27`
            if (e.keyCode == 27) {

                // call the close and reset function
                close_video_modal();

            }
        });
    }
    toggle_video_modal();

    /*
    |--------------------------------------------------------------------------
    | Smooth Scroll
    |--------------------------------------------------------------------------
    */

    $('.js-page-scroll').on('click', function(event) {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            let target = $(this.hash),
                speed = $(this).data("speed") || 800;
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top - 0
                }, speed);
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Spoiler Text
    |--------------------------------------------------------------------------
    */

    var containerHeight = document.querySelectorAll(".js-spoiler-inner");
    var uncoverLink = document.querySelectorAll(".js-spoiler-more");

    for(let i = 0; i < containerHeight.length; i++){
        let openData = uncoverLink[i].dataset.open;
        let closeData = uncoverLink[i].dataset.close;
        let curHeight = containerHeight[i].dataset.height;

        uncoverLink[i].innerHTML = openData;
        containerHeight[i].style.maxHeight = curHeight + "px";

        uncoverLink[i].addEventListener("click", function(){
            if(containerHeight[i].classList.contains("is-open")){

                containerHeight[i].classList.remove("is-open");

                uncoverLink[i].innerHTML = openData;

                containerHeight[i].style.maxHeight = curHeight + "px";

            } else {
                containerHeight[i].removeAttribute("style");

                containerHeight[i].classList.add("is-open");

                uncoverLink[i].innerHTML = closeData;

            }
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Entry Slider
    |--------------------------------------------------------------------------
    */

	let entrySlider = new Swiper('.js-entry-slider', {
		speed: 600,
		mousewheel: false,
		loop: true,
		autoplay: {
			delay: 2000,
			disableOnInteraction: false,
		},
		spaceBetween: 30,
        pagination: {
            el: '.js-entry-slider-pagination',
            clickable: true,
        },
		slidesPerView: 1,
	});

    /*
    |--------------------------------------------------------------------------
    | Products Slider
    |--------------------------------------------------------------------------
    */

    let productsSlider = new Swiper('.js-products-slider', {
        speed: 600,
        mousewheel: false,
        loop: false,
        touchRatio: 0,
        spaceBetween: 30,
        slidesPerView: 4,
        breakpoints: {
            1200: {
                slidesPerView: 3,
                spaceBetween: 30,
                touchRatio: 0,
            },
            992: {
                slidesPerView: 2.2,
                spaceBetween: 20,
                touchRatio: 1,
            },
            768: {
                slidesPerView: 2.2,
                spaceBetween: 20,
                touchRatio: 1,
            },
            640: {
                slidesPerView: 1.2,
                spaceBetween: 15,
                touchRatio: 1,
            },
            320: {
                slidesPerView: 1.1,
                spaceBetween: 15,
                touchRatio: 1,
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Seen Slider
    |--------------------------------------------------------------------------
    */

    let seenSlider = new Swiper('.js-seen-slider', {
        speed: 600,
        mousewheel: false,
        loop: false,
        touchRatio: 0,
        spaceBetween: 30,
        slidesPerView: 3,
        navigation: {
            nextEl: '.js-seen-slider-next',
            prevEl: '.js-seen-slider-prev',
        },
        breakpoints: {
            1200: {
                slidesPerView: 2,
                spaceBetween: 30,
                touchRatio: 0,
            },
            992: {
                slidesPerView: 2.3,
                spaceBetween: 20,
                touchRatio: 1,
            },
            768: {
                slidesPerView: 2.2,
                spaceBetween: 20,
                touchRatio: 1,
            },
            640: {
                slidesPerView: 1.2,
                spaceBetween: 15,
                touchRatio: 1,
            },
            320: {
                slidesPerView: 1.1,
                spaceBetween: 15,
                touchRatio: 1,
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Main Banner
    |--------------------------------------------------------------------------
    */

    var mainBanner = new Swiper('.js-main-banner', {
        speed: 600,
        mousewheel: false,
        loop: true,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
        },
        spaceBetween: 30,
        pagination: {
            el: '.js-main-banner-pagination',
            clickable: true,
        },
        slidesPerView: 1
    });

    /*
    |--------------------------------------------------------------------------
    | Product Slider
    |--------------------------------------------------------------------------
    */

    var productSlider = new Swiper('.js-product-slider', {
        spaceBetween: 5,
        slidesPerView: 1,
        speed: 700,
        autoHeight: true,
        navigation: {
            nextEl: '.js-product-card-next',
            prevEl: '.js-product-card-prev',
        },
        thumbs: {
            swiper: {
                el: '.js-product-thumbs',
                spaceBetween: 10,
                slidesPerView: 4,
                freeMode: true,
                watchSlidesVisibility: true,
                watchSlidesProgress: true,
                breakpoints: {
                    1200: {
                        slidesPerView: 4,
                    },
                    1024: {
                        slidesPerView: 3,
                    },
                    768: {
                        slidesPerView: 3,
                    },
                    640: {
                        slidesPerView: 3,
                    },
                    320: {
                        slidesPerView: 3,
                    }
                }
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Mega Menu
    |--------------------------------------------------------------------------
    */

    $(".js-mega-menu").hover(function(){
        var dropdownMenu = $(this).children(".mega-menu");
        if(dropdownMenu.is(":visible")){
            dropdownMenu.parent().addClass("--open");
            $(".shadow-overlay").addClass("--show");
        } else {
            dropdownMenu.parent().removeClass("--open");
            $(".shadow-overlay").removeClass("--show");
        }
    });

    $(".mega-menu__item").hover(function(){
        var dropdownMenu2 = $(this).children(".mega-menu__content");
        if(dropdownMenu2.is(":visible")){
            dropdownMenu2.parent().toggleClass("--open");
        }
    });
    /*
    |--------------------------------------------------------------------------
    | Light Gallery
    |--------------------------------------------------------------------------
    */

	$('.js-lg').lightGallery({
		selector: ".js-lg-item",
	});


    /*
    |--------------------------------------------------------------------------
    | Bootstrap Tooltip
    |--------------------------------------------------------------------------
    */

    $('[data-toggle="tooltip"]').tooltip();
    // colored tooltip
    $('[data-toggle="tooltip-primary"]').tooltip({
        template: '<div class="tooltip tooltip-primary" role="tooltip"><div class="arrow"><\/div><div class="tooltip-inner"><\/div><\/div>'
    });
    $('[data-toggle="tooltip-secondary"]').tooltip({
        template: '<div class="tooltip tooltip-secondary" role="tooltip"><div class="arrow"><\/div><div class="tooltip-inner"><\/div><\/div>'
    });

    /*
    |--------------------------------------------------------------------------
    | Clipboard.js
    |--------------------------------------------------------------------------
    */

    if ($('.clipboard-icon').length) {
        var clipboard = new ClipboardJS('.clipboard-icon');

        $('.clipboard-icon').attr('data-toggle', 'tooltip').attr('title', 'Copy to clipboard');


        $('[data-toggle="tooltip"]').tooltip();

        clipboard.on('success', function(e) {
            e.trigger.classList.value = 'clipboard-icon btn-current'
            $('.btn-current').tooltip('hide');
            e.trigger.dataset.originalTitle = 'Copied';
            $('.btn-current').tooltip('show');
            setTimeout(function(){
                $('.btn-current').tooltip('hide');
                e.trigger.dataset.originalTitle = 'Copy to clipboard';
                e.trigger.classList.value = 'clipboard-icon'
            },1000);
            e.clearSelection();
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Select2
    |--------------------------------------------------------------------------
    */

    $(".select2").each(function() {
        $(this).select2({
            placeholder: $(this).attr('data-placeholder'),
            width: '100%',
            minimumResultsForSearch: -1
        });
    });

    $(".select2-search").each(function() {
        $(this).select2({
            placeholder: $(this).attr('data-placeholder'),
            width: '100%',
            allowClear: false,
            minimumResultsForSearch: 5
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Amaze UI Datetime Picker
    | https://github.com/amazeui/datetimepicker
    |--------------------------------------------------------------------------
    */

    /**
     * Russian translation for bootstrap-datetimepicker
     * Victor Taranenko <darwin@snowdale.com>
     */
    ;(function($){
        $.fn.datetimepicker.dates['ru'] = {
            days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
            daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб", "Вск"],
            daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
            months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
            today: "Сегодня",
            suffix: [],
            meridiem: []
        };
    }(jQuery));

    // AmazeUI Datetimepicker
    $('.js-datetimepicker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true,
        language: 'ru'
    });

    /*
    |--------------------------------------------------------------------------
    | Back to Top
    |--------------------------------------------------------------------------
    */

    $(window).on("scroll", function(e) {
        if ($(this).scrollTop() > 0) {
            $('.js-back-to-top').fadeIn('slow');
        } else {
            $('.js-back-to-top').fadeOut('slow');
        }
    });

    $(".js-back-to-top").on("click", function(e) {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});
